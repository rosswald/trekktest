<?php declare(strict_types=1);
namespace TrekkTest;


class Rover
{
    private int $x;
    private int $y;
    private int $direction;
    private string $instructions;
    private Mars $mars;
    const DIRECTION_N = 0,
        DIRECTION_E = 90,
        DIRECTION_S = 180,
        DIRECTION_W = 270;


    public function __construct(int $x, int $y, string $direction, Mars $mars)
    {
        $this->mars = $mars;
        if (($x > $this->mars->getMaxX())
            || ($x < 0)
            || ($y > $this->mars->getMaxY())
            || ($y < 0)) {
            throw new \Exception("Rover starting position out of bounds: $x, $y");
        }

        $this->x = $x;
        $this->y = $y;

        if (strlen($direction) > 1) {
            throw new \Exception("Invalid direction length: " . strlen($direction));
        }

        if (!in_array(strtoupper($direction), ['N', 'E', 'S', 'W'])) {
            throw new \Exception("Invalid direction: $direction");
        }

        $this->direction = constant("self::DIRECTION_" . strtoupper($direction));

    }

    public function status(): string
    {
        $direction = array_flip((new \ReflectionClass(__CLASS__))->getConstants())[$this->direction];

        return $this->x . " " . $this->y . " " . substr($direction, strlen($direction) - 1, 1);
    }

    public function setInstructions(string $instructions): Rover
    {
        $this->instructions = $instructions;
        return $this;
    }

    public function getInstructions(): string
    {
        return $this->instructions;
    }

    public function run(): Rover
    {
        for ($i = 0, $cnt = strlen($this->instructions); $i < $cnt; $i++) {
            $charAt = strtoupper(substr($this->instructions, $i, 1));
            if (!in_array($charAt, ['L', 'R', 'M'])) {
                throw new \Exception("Invalid instruction: $charAt");
            }

            if ($charAt == 'M') {
                $this->move();
            } else {
                $this->rotate($charAt);
            }
        }

        return $this;
    }

    private function move(): Rover
    {
        switch ($this->direction) {
            case self::DIRECTION_N :
                if ($this->y < $this->mars->getMaxY()) {
                    $this->y++;
                }
                break;
            case self::DIRECTION_E :
                if ($this->x < $this->mars->getMaxX()) {
                    $this->x++;
                }
                break;
            case self::DIRECTION_S :
                if ($this->y > 0) {
                    $this->y--;
                }
                break;
            case self::DIRECTION_W :
                if ($this->x > 0) {
                    $this->x--;
                }
                break;
        }

        return $this;
    }

    private function rotate(string $direction): Rover
    {
        if ($direction == 'L') {
            $this->direction -= 90;
        } else { // R
            $this->direction += 90;
        }

        if ($this->direction < 0) {
            $this->direction += 360;
        } else if ($this->direction >= 360) {
            $this->direction -= 360;
        }

        return $this;
    }
}