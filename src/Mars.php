<?php declare(strict_types=1);
namespace TrekkTest;


class Mars
{
    private int $maxX = 0;
    private int $maxY = 0;
    private static Mars $instance;

    private function __construct() {}

    public static function getInstance(): Mars
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    public function setUpperRightCoords(int $x, int $y): void
    {
        $this->maxX = $x;
        $this->maxY = $y;
    }

    public function getMaxX(): int
    {
        return $this->maxX;
    }

    public function getMaxY(): int
    {
        return $this->maxY;
    }
}