<?php declare(strict_types=1);
namespace TrekkTest;

class App
{
    private array $input;
    private Mars $mars;
    private array $rovers;
    
    public function run()
    {
        try {
            $this->setInput($this->readStdin())
                ->parseInput()
                ->runRovers()
                ->printOutput();
        } catch (\Exception $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function readStdin(): array
    {
        $input = [];
        while (!feof(STDIN)) {
            $input[] = trim(fgets(STDIN));
        }

        return $input;
    }

    public function setInput(array $input): App
    {
        $this->input = $input;
        return $this;
    }

    public function parseInput(): App
    {
        for ($i = 0, $cnt = count($this->input); $i < $cnt; $i++) {
            if ($i == 0) {
                $dimensions = explode(' ', $this->input[$i]);
                $this->setMars((int)$dimensions[0], (int)$dimensions[1]);
            } else if (($i % 2 == 1) && (strlen($this->input[$i]) > 0)) {
                $roverParams = explode(' ', $this->input[$i]);
                $rover = $this->addRover((int)$roverParams[0], (int)$roverParams[1], $roverParams[2]);
            } else if (isset($rover)) {
                $rover->setInstructions($this->input[$i]);
                unset($rover);
            }
        }

        return $this;
    }

    public function runRovers(): App
    {
        foreach ($this->rovers as $rover) {
            $rover->run();
        }

        return $this;
    }

    public function printOutput(): App
    {
        /** @var Rover $rover */
        foreach ($this->rovers as $rover) {
            echo $rover->status() . "\n";
        }

        return $this;
    }

    public function setMars(int $x, int $y): Mars
    {
        $this->mars = Mars::getInstance();
        $this->mars->setUpperRightCoords($x, $y);
        return $this->mars;
    }

    public function addRover(int $x, int $y, string $direction): Rover
    {
        $rover = new Rover($x, $y, $direction, $this->mars);
        $this->rovers[] = $rover;
        return $rover;
    }

    public function getMars(): Mars
    {
        return $this->mars;
    }

    public function getRovers(): array
    {
        return $this->rovers;
    }
}