<?php
use PHPUnit\Framework\TestCase;
use TrekkTest\Mars;
use TrekkTest\Rover;
use TrekkTest\App;

class AppTest extends TestCase
{
    protected $inputFile;
    protected $mars;

    protected function setUp(): void
    {
        $this->inputFile = [
            '5 5',
            '1 2 N',
            'LMLMLMLMM',
            '3 3 E',
            'MMRMMRMRRM',
        ];
    }

    public function testParseInputForMars()
    {
        $app = new App();
        $app->setInput($this->inputFile);
        $app->parseInput();
        $this->assertEquals(5, $app->getMars()->getMaxX());
        $this->assertEquals(5, $app->getMars()->getMaxY());
    }

    public function testParseInputForRovers()
    {
        $rover1 = (new Rover(1, 2, 'N', Mars::getInstance()))
            ->setInstructions('LMLMLMLMM');
        $rover2 = (new Rover(3, 3, 'E', Mars::getInstance()))
            ->setInstructions('MMRMMRMRRM');
        $app = new App();
        $app->setInput($this->inputFile);
        $app->parseInput();
        $this->assertEquals($rover1->status(), $app->getRovers()[0]->status());
        $this->assertEquals($rover1->getInstructions(), $app->getRovers()[0]->getInstructions());
        $this->assertEquals($rover2->status(), $app->getRovers()[1]->status());
        $this->assertEquals($rover2->getInstructions(), $app->getRovers()[1]->getInstructions());
    }

    public function testRunRovers()
    {
        $app = new App();
        $app->setInput($this->inputFile);
        $app->parseInput();
        $app->runRovers();
        $this->assertEquals('1 3 N', $app->getRovers()[0]->status());
        $this->assertEquals('5 1 E', $app->getRovers()[1]->status());
    }

}