<?php
use PHPUnit\Framework\TestCase;
use TrekkTest\Mars;
use TrekkTest\Rover;

class RoverTest extends TestCase
{
    protected $mars;

    protected function setUp(): void
    {
        $this->mars = Mars::getInstance();
        $this->mars->setUpperRightCoords(5,5);
    }

    public function testOutOfBounds()
    {
        $this->expectExceptionMessage('Rover starting position out of bounds');
        $rover = new Rover(11, 12, 'E', $this->mars);
    }

    public function testInvalidDirection()
    {
        $this->expectExceptionMessage('Invalid direction');
        $rover = new Rover(1, 2, 'WW', $this->mars);
    }

    public function testInvalidDirection2()
    {
        $this->expectExceptionMessage('Invalid direction');
        $rover = new Rover(1, 2, 'Q', $this->mars);
    }

    public function testMoveEast()
    {
        $rover = new Rover(1, 1, 'E', $this->mars);
        $rover->setInstructions('MMM');
        $rover->run();
        $this->assertEquals($rover->status(), '4 1 E');
    }

    public function testMoveEastBorder()
    {
        $rover = new Rover(1, 1, 'E', $this->mars);
        $rover->setInstructions('MMMMMMMM');
        $rover->run();
        $this->assertEquals($rover->status(), '5 1 E');
    }

    public function testMoveWest()
    {
        $rover = new Rover(4, 1, 'W', $this->mars);
        $rover->setInstructions('MMM');
        $rover->run();
        $this->assertEquals($rover->status(), '1 1 W');
    }

    public function testMoveWestBorder()
    {
        $rover = new Rover(4, 1, 'W', $this->mars);
        $rover->setInstructions('MMMMMMMM');
        $rover->run();
        $this->assertEquals($rover->status(), '0 1 W');
    }

    public function testMoveNorth()
    {
        $rover = new Rover(1, 1, 'N', $this->mars);
        $rover->setInstructions('MMM');
        $rover->run();
        $this->assertEquals($rover->status(), '1 4 N');
    }

    public function testMoveNorthBorder()
    {
        $rover = new Rover(1, 1, 'N', $this->mars);
        $rover->setInstructions('MMMMMMMM');
        $rover->run();
        $this->assertEquals($rover->status(), '1 5 N');
    }

    public function testMoveSouth()
    {
        $rover = new Rover(1, 4, 'S', $this->mars);
        $rover->setInstructions('MMM');
        $rover->run();
        $this->assertEquals($rover->status(), '1 1 S');
    }

    public function testMoveSouthBorder()
    {
        $rover = new Rover(1, 4, 'S', $this->mars);
        $rover->setInstructions('MMMMMMMM');
        $rover->run();
        $this->assertEquals($rover->status(), '1 0 S');
    }

    public function testRotateL1()
    {
        $rover = new Rover(1, 1, 'E', $this->mars);
        $rover->setInstructions('L');
        $rover->run();
        $this->assertEquals($rover->status(), '1 1 N');
    }

    public function testRotateL3()
    {
        $rover = new Rover(1, 1, 'E', $this->mars);
        $rover->setInstructions('LLL');
        $rover->run();
        $this->assertEquals($rover->status(), '1 1 S');
    }

    public function testRotateL6()
    {
        $rover = new Rover(1, 1, 'E', $this->mars);
        $rover->setInstructions('LLLLLL');
        $rover->run();
        $this->assertEquals($rover->status(), '1 1 W');
    }

    public function testRotateR1()
    {
        $rover = new Rover(1, 1, 'E', $this->mars);
        $rover->setInstructions('R');
        $rover->run();
        $this->assertEquals($rover->status(), '1 1 S');
    }

    public function testRotateR3()
    {
        $rover = new Rover(1, 1, 'E', $this->mars);
        $rover->setInstructions('RRR');
        $rover->run();
        $this->assertEquals($rover->status(), '1 1 N');
    }

    public function testRotateR6()
    {
        $rover = new Rover(1, 1, 'E', $this->mars);
        $rover->setInstructions('RRRRRR');
        $rover->run();
        $this->assertEquals($rover->status(), '1 1 W');
    }






}